package com.yurifbecker.apetite.restauantelegal;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.yurifbecker.apetite.restauantelegal.Consumidor.Consumidor;
import com.yurifbecker.apetite.restauantelegal.Empreendedor.Empreendedor;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button mButtonEmpreendedor, mButtonConsumidor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mButtonConsumidor = (Button) findViewById(R.id.buttonConsumidor);
        mButtonEmpreendedor = (Button) findViewById(R.id.buttonEmpreendedor);

        mButtonConsumidor.setOnClickListener(this);
        mButtonEmpreendedor.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.buttonConsumidor){
            Intent mIntent = new Intent(this,Consumidor.class);
            startActivity(mIntent);

        }else if(v.getId() == R.id.buttonEmpreendedor){
            Intent mIntent = new Intent(this,Empreendedor.class);
            startActivity(mIntent);
        }
    }
}
